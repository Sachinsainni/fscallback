let fs = require('fs');

function problem2(lipsumPath) {
    function readLipsumFile(lipsumPath) {
        fs.readFile(lipsumPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("Read lipsumFile.txt")
                writeUpperCase(data);
            }
        })
    }
    readLipsumFile(lipsumPath);

    function writeFileName(file) {
        let fileName = "fileName.txt";
        let filePath = `/home/sachin/project/fsCallback/data/${fileName}`;
        fs.appendFile(filePath, ' ' + file, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`file ${file} written in ${fileName} file`);
            }
        })
    }

    function writeUpperCase(data) {
        let inUppercase = data.toUpperCase();
        let fileName = 'upperCase.txt';
        let filePath = `/home/sachin/project/fsCallback/data/${fileName}`;
        fs.writeFile(filePath, inUppercase, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`File ${fileName} write successfully`);
            }
        });
        writeFileName(fileName);
        writeLowerCase(inUppercase);

    }
    function writeLowerCase(data) {
        let inLowercase = data.toLowerCase()
            .split('.')
            .join('\n');
        let fileName = 'lowerCase.txt';
        let filePath = `/home/sachin/project/fsCallback/data/${fileName}`;
        fs.writeFile(filePath, inLowercase, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`File ${fileName} write successfully`);
            }
        });
        writeFileName(fileName);
        sortingContent(filePath);
    }

    function sortingContent(pathOfFile) {
        fs.readFile(pathOfFile, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("read the new file");
                let dataSorted = data.split(' ')
                    .sort().join(' ');
                let fileName = "sortedContent.txt";
                let filePath = `/home/sachin/project/fsCallback/data/${fileName}`;
                fs.writeFile(filePath, dataSorted, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`file ${fileName} write successfully.`)
                    }
                })
                writeFileName(fileName);

            }
        })
    }
    setTimeout(() => {
        let fileName = 'fileName.txt'
        let pathOfDir = '/home/sachin/project/fsCallback/data/'
        let filePath = `${pathOfDir}${fileName}`
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("fileName.txt read successfully");
                let files = data.trim()
                    .split(' ');
                let deletedFile = files.map((item) => {
                    fs.unlink(`${pathOfDir}${item}`, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${item} file deleted successfully`);
                        }
                        return item;
                    })
                })
            }
        })
    }, 5000)
}

module.exports = problem2;